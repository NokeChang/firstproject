# Log分析器
將設備日誌寫入資料庫，利用資料庫強大的搜尋與統計能力，提供日誌查詢與資料統計報告。
## LogAnalyzer.dll
### ReportWidget
![輸出報告元件](Images/reportwidget.png)
### QueryWidget
![輸出報告元件](Images/querywidget.png)
### LogDB
* 應用程式前處理：設定初始報告檔案位置；若執行環境沒有資料庫，自動建立資料庫在執行檔資料夾。
```c#
public void SetDefaultReportPath()
public void CreateDbIfNotExist()
```
* 寫入訊息到資料庫
```c#
//操作訊息、警報訊息
public void LogSayDb(string mMsg)
//生產、復歸、閒置、手動、暫停時間
public void LogSayDb(ulong runSecond, ulong homeTM, ulong idleTM, ulong manualTM, ulong stopSecond)
//生產數量與產能
public void LogSayDb(int pieces, float capacity)
```
* 關閉應用程式前：若資料庫檔案>3GB，資料庫備份；壓縮資料庫log檔。
```c#
public void RunBeforeClose()
```
## 下載並安裝MS SqlLocalDB
<https://www.microsoft.com/zh-tw/sql-server/sql-server-editions-express>

下載SQL Server 2017 Express LocalDB，安裝時選擇LocalDB即可。
>輔助工具SSMS
<https://docs.microsoft.com/zh-tw/sql/ssms/sql-server-management-studio-ssms?view=sql-server-ver15>
## 報告輸出
* 時間範圍為所選開始日期的**00:00:00**到結束日期的**00:00:00**，例如選擇2019/11/3~2019/11/4，為2019/11/3全天。
* 區間分為每日、每周、每月，用於周期統計，例如設備運轉指標、稼動率、產能&產量。
* 範圍天數若不足所選的輸出區間的倍數，會自動補齊天數。
例如：日期範圍為2019/12/4~2019/12/13，區間為每周，則會統計資料至2019/12/18 00:00:00，產生兩周的數據。
## 報告檔案
檔案名稱：**設備效率評估報告.xls**，使用Excel 97~2003版本，共1工作頁：綜合報告， 輸出五項統計報告。
* Top5 Jam
* 模組警報統計
* 設備運轉指標 (MTBA、MTBF、MTTR)
* 稼動率
* 產量&產能
## 編譯後
除了相依的dll外，**ReportConfig.xml**與**設備效率評估報告.xls**兩個檔案會被複製到執行檔所在資料夾。
# 測試表單WidgetTest
## 產生測試用資料
* 可定時產生資料。
* 可一次產生大量資料。
## 刪除專案
* 因資料庫檔案位於專案內，必須用表單按鈕"刪除資料庫"，才可將資料庫完全移除。
* 按下後會自動關閉程式，即可將專案刪除。